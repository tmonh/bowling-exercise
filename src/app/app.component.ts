import { Component } from '@angular/core';
import { GlobalConstants } from './common/global-constants';
import { faBowlingBall } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  //Labels
  title = GlobalConstants.title;

  //Icons
  faBowlingBall = faBowlingBall;
}
