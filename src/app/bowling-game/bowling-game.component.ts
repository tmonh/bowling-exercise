import { Component, OnInit } from '@angular/core';
import { GlobalConstants } from '../common/global-constants';
import { Frame } from '../models/frame.model';
import { Score } from '../models/score.model';
import { faBowlingBall, faRedo } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-bowling-game',
  templateUrl: './bowling-game.component.html',
  styleUrls: ['./bowling-game.component.scss']
})
export class BowlingGameComponent implements OnInit {
  

  // Labels
  rollBallLbl = GlobalConstants.rollBall;
  rollLbl = GlobalConstants.roll;
  restartLbl = GlobalConstants.restart;
  scoreLbl = GlobalConstants.score;
  frameLbl = GlobalConstants.frame;
  nextFrameLbl = GlobalConstants.nextFrame;

  //Icons
  faBowlingBall = faBowlingBall;
  faRedo = faRedo;
  
   // Properties
  private frames!: Frame[];
  private previousFrame!: Frame;
  private currentFrame!: Frame;
  public gameOver: boolean = false;
  public gameOverMessage: string = GlobalConstants.gameOver;
  public frameComplete: boolean = false;
  public scoreDisplay: Score[] = [];
  public rollDisplay: string[] = [];

  constructor() {
   }

  ngOnInit(): void {
    this.newGame();
  }

  // Get the current frame position
  get currentFramePosition(): number {
    return this.frames?.length ?? 1;
  }

  // Validate if the user can roll the ball if frame is not complete and game is not over
  get rollBallEnabled(): boolean {
    return this.currentFrame != null 
    && !this.frameComplete
    && !this.gameOver;
  }

  // Is this the Last Frame of the game?
  get isLastFrame(): boolean {
    return this.frames.length == GlobalConstants.maxFrames;
  }

  //#region Methods
  newGame() {
    this.gameOver = false;
    this.gameOverMessage = GlobalConstants.gameOver;
    this.scoreDisplay = [];
    this.frames = [];
    this.newFrame(true);
  }

  // Conclude Game and Display Total Score
  completeGame() {
    this.completeFrame();
    let totalScore = 0;
    this.gameOver = true;

    //Count Total Score
    this.frames.forEach(frame => {
      totalScore += frame.frameScore;
    });

    //Update Display with Total Score and Game Over
    this.gameOverMessage = '<br>' + GlobalConstants.finalScore + totalScore + '<br>' + GlobalConstants.gameOver;
  }

  // Complete Frame and ready for new Frame
  completeFrame() {
    // Tally Scores
    this.currentFrame.rollScore.forEach(score => {
      this.currentFrame.frameScore += score;
    });

    // Update last frame data if taking account of strike/spare
    if (this.frames.length > 1) {
      if(this.previousFrame.spare) {
        this.previousFrame.frameScore += this.currentFrame.rollScore[0];
      }
      else if(this.previousFrame.strike) {
        this.currentFrame.rollScore.slice(0, 2).forEach(score => {
          this.previousFrame.frameScore += score;
        })
      }
    }

    // Update Score Display
    let score = {
      position: this.currentFramePosition,
      roll1: this.currentFrame.rollScore[0] ?? 0,
      roll2: this.currentFrame.rollScore[1] ?? 0,
      roll3: this.currentFrame.rollScore.length > 2 ? this.currentFrame.rollScore[2] : null
    }
    this.scoreDisplay.push(score);
    this.scoreDisplay = this.scoreDisplay.slice();

    this.frameComplete = true;
  }

  // Setting up New Frame
  newFrame(newGame: boolean = false) {
    this.rollDisplay = [];
    this.frameComplete = false;
    
    // Update Internal Frame Data
    this.currentFrame = {
        rollScore: [],
        frameScore: 0,
        pinsLeft: GlobalConstants.maxPins,
        spare: false,
        strike: false
    };
    this.frames.push(this.currentFrame);

    // Get Previous Frame Data
    if(this.frames.length > 1) {
      this.previousFrame = this.frames[this.frames.length - 2];
    }
  }

  // Get display string for current roll results
  getRollResults(): string{
    let result = '';
    let index = this.currentFrame.rollScore.length - 1;
    let score = this.currentFrame.rollScore[index];
    let previousScore = index > 0 ? this.currentFrame.rollScore[index - 1] : null;
    if (score == 1) {
      result = score + " " + GlobalConstants.pinHit;
    } 
    else {
      result = score + " " + GlobalConstants.pinsHit;
    }
    
   // Validate for a Strike
   if(score == GlobalConstants.maxPins && (previousScore == GlobalConstants.maxPins || previousScore == null)) {
     result += " " + GlobalConstants.strike + "!"; 
   }
   // Validate for Spare, third row on last frame cannot result in a spare
   else if(previousScore != null && (previousScore + score == GlobalConstants.maxPins) && index != 2) {
    result += " " + GlobalConstants.spare + "!"; 
   }

    return result;
  }

  // Roll Bowling Ball
  rollBall() {
    let pinsHit = this.pinsHit(this.currentFrame.pinsLeft);

    this.currentFrame.pinsLeft -= pinsHit;
    this.currentFrame.rollScore.push(pinsHit);

    // Rules for last frame scoring, no need to take account of strike or spare state
    if(this.isLastFrame) {
      if(this.currentFrame.pinsLeft == 0) {
        if(this.currentFrame.rollScore.length < 3) {
          this.currentFrame.pinsLeft = GlobalConstants.maxPins;
        }
        else {
          this.completeGame();
        }
      }
      else if(this.currentFrame.rollScore.length >= 2) {
        this.completeGame();
      }
    }
    // Rules for regular frame scoring, take account if frame is a strike or spare
    else{
      if(this.currentFrame.pinsLeft == 0) {
        if(this.currentFrame.rollScore.length == 1) {
          this.currentFrame.strike = true;
        }
        else{
          this.currentFrame.spare = true;
        }
        this.completeFrame();
      }
      else if(this.currentFrame.rollScore.length >= 2) {
        this.completeFrame();
      }
    }

    this.rollDisplay.push(this.getRollResults());
  }

  // Random Number Generator for getting pins hit by bowling ball
  pinsHit(maxValue: number): number {
    return Math.floor(Math.random() * (maxValue + 1));
  }

  //#endregion Methods

}
