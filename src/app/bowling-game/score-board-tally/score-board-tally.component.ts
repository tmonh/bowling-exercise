import { Component, Input, OnInit, SimpleChange, SimpleChanges, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { GlobalConstants } from 'src/app/common/global-constants';
import { Score } from 'src/app/models/score.model';

@Component({
  selector: 'app-score-board-tally',
  templateUrl: './score-board-tally.component.html',
  styleUrls: ['./score-board-tally.component.scss']
})
export class ScoreBoardTallyComponent implements OnInit {
  @Input() scoreDisplay!: Score[];

   // Table Properties
   public displayedColumns: string[] = ['position', 'roll1', 'roll2', 'roll3']

   // Labels
   rollLbl = GlobalConstants.roll;
   frameLbl = GlobalConstants.frame;

  constructor() { }

  ngOnInit(): void {
    
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

}
