import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoreBoardTallyComponent } from './score-board-tally.component';

describe('ScoreBoardTallyComponent', () => {
  let component: ScoreBoardTallyComponent;
  let fixture: ComponentFixture<ScoreBoardTallyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScoreBoardTallyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoreBoardTallyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
