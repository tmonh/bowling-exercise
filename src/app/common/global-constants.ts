export class GlobalConstants {
    // String Label Constants
    public static title: string = "Bowling Game";

    public static frame: string = "Frame";

    public static roll: string = "Roll";

    public static rollBall: string = "Roll Ball";

    public static score: string = "Score";

    public static finalScore: string = "Final Score: ";

    public static restart: string = "Restart";

    public static spare: string = "Spare";

    public static strike: string = "Strike";

    public static pinsHit: string = "Pins Hit"

    public static pinHit: string = "Pin Hit"

    public static nextFrame: string = "Next Frame"

    public static gameOver: string = "Game Over";

    // Value Constants
    public static maxPins: number = 10;

    public static maxFrames: number = 10;
}
