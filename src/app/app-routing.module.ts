import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BowlingGameComponent } from './bowling-game/bowling-game.component';

const routes: Routes = [
  {path: '', component: BowlingGameComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
