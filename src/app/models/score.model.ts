export interface Score {

    position: number;

    roll1: number; 

    roll2: number;

    roll3: number | null;
}
