export interface Frame {

    rollScore: number[];

    frameScore: number;
    
    pinsLeft: number;

    spare: boolean;

    strike: boolean;
}
